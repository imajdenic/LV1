# -*- coding: utf-8 -*-
"""
Created on Tue Nov 03 14:15:41 2015

@author: ilija
"""
#sa try privjera ispravnosti unosa brojeva
try:
    #unos ocijena i izvršenje zadatka pomoću if petlje
    ocijena=input("unos=")
    if ocijena>1.0 or ocijena<0.0:
        print 'broj je izvan intervala!'
    elif ocijena>=0.9:
        print 'A'
    elif ocijena>=0.8:
        print 'B'
    elif ocijena>=0.7:
        print 'C'
    elif ocijena>=0.6:
        print 'D' 
    elif ocijena<0.6 or ocijena>0.0:
        print 'F'
except:
    print ("pogrešan unos!")
